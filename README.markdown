# Schedule Tool for WWE

Schedule tool to manage everyday tasks of WWE game.

##  Features

- Basic features base on Bonfire project (http://cibonfire.com)
- Material theme clone from https://github.com/lesydat/Bonfire/tree/material-theme

These feature will be develop for this project:
- Login by G+
- Schedule calendar for manage tasks doing everyday

## The Team

Gear Inc. Tool Team