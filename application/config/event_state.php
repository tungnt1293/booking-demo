<?php defined('BASEPATH') || exit('No direct script access allowed');

$config['color_state'] = [
	'Planning' => 'cyan',
	'Implementing' => 'blue',
	'Ready for QA' => 'orange',
	'QA in progress' => 'lime',
	'QA approved' => 'green',
	'Deployed' => 'brown',
	'Canceled' => 'gray'
];