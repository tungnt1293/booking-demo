<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Bonfire
 *
 * An open source project to allow developers to jumpstart their development of
 * CodeIgniter applications.
 *
 * @package   Bonfire
 * @author    Bonfire Dev Team
 * @copyright Copyright (c) 2011 - 2014, Bonfire Dev Team
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @link      http://cibonfire.com
 * @since     Version 1.0
 * @filesource
 */

/**
 * Home controller
 *
 * The base controller which displays the homepage of the Bonfire site.
 *
 * @package    Bonfire
 * @subpackage Controllers
 * @category   Controllers
 * @author     Bonfire Dev Team
 * @link       http://guides.cibonfire.com/helpers/file_helpers.html
 *
 */
class Home extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('application');
		$this->load->library('Template');
		$this->load->library('Assets');
		$this->lang->load('application');
		$this->load->library('events');

        $this->load->library('installer_lib');
        if (! $this->installer_lib->is_installed()) {
            $ci =& get_instance();
            $ci->hooks->enabled = false;
            redirect('install');
        }

        // Make the requested page var available, since
        // we're not extending from a Bonfire controller
        // and it's not done for us.
        $this->requested_page = isset($_SESSION['requested_page']) ? $_SESSION['requested_page'] : null;
	}

	//--------------------------------------------------------------------

	/**
	 * Displays the homepage of the Bonfire app
	 *
	 * @return void
	 */
	public function index()
	{
		$this->load->library('users/auth');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->set_current_user();

		// Get list services 
		$this->load->model('services_model');
		$list_services = $this->services_model
							->select('id, service_name, price')
							->where('deleted', 0)
							->order_by('service_name')
							->find_all();

		// Get list vinpearl 
		$this->load->model('Vinpearl_model');
		$list_vinpearl = $this->Vinpearl_model
							->select('id, vinpearl, code')
							->where('deleted', 0)
							->order_by('vinpearl')
							->find_all();

		// Get list employees 
		$this->load->model('employees_model');
		$list_employees = $this->employees_model
							->select('id, fullname, employee_code')
							->where('deleted', 0)
							->order_by('fullname')
							->find_all();

		// random a booking id for testing
		$random_booking_id = $this->generateRandomString(10);

		Assets::add_js('codeigniter-csrf.js');
		Assets::add_js('vendors/bootstrap-growl/bootstrap-growl.min.js');
		Assets::add_js('vendors/QR_code/jquery-qrcode-0.17.0.min.js');
		Assets::add_js('themes/default/js/home.js');
		Assets::add_css('themes/default/css/home.css');
		Template::set('list_services', $list_services);
		Template::set('list_vinpearl', $list_vinpearl);
		Template::set('list_employees', $list_employees);
		Template::set('random_booking_id', $random_booking_id);
		Template::render();
	}

	public function submit_booking() {
		$this->load->library('form_validation');
		$this->load->model('booking_model');
		$this->load->model('booking_service_model');
		$this->load->model('services_model');
		$response = [
			'type' => 'error',
			'message' => 'submit error'
		];

		$data = $this->input->post();
		// Just only required checking
		if (!$data['booking_code'] || !$data['fullname'] || 
		!$data['id_card_number'] || !$data['booking_date'] || !$data['seller']) {
			$response['message'] = 'Hãy điền đẩy đủ thông tin vào các trường.';
			echo json_encode($response); die; 
		}
		
		if (!isset($data['services']) || !$data['services'] ) {
			$response['message'] = 'Hãy chọn ít nhất 1 dịch vụ';
			echo json_encode($response); die; 
		}

		foreach ($data['services'] as $service) {
			if (!$service['service'] || !$service['quanlity']) {
				$response['message'] = 'Hãy chọn ít nhất 1 dịch vụ';
				echo json_encode($response); die; 
			}
		}

		// Check is_numeric id card
		if (!is_numeric($data['id_card_number'])) {
			$response['message'] = 'Chứng minh thư phải là số.';
			echo json_encode($response); die; 
		}

		// Check booking code exists
		$check_booking_id = $this->booking_model->select('id')->where('booking_code', $data['booking_code'])->find_all();
		if ($check_booking_id) {
			$response['message'] = 'Mã Booking đã tồn tại, vui lòng chọn lại.';
			echo json_encode($response); die; 
		}
		// Save booking data
		$data_booking = [];
		$data_booking['booking_code'] = $data['booking_code'];
		$data_booking['booking_date'] = $data['booking_date'];
		$data_booking['fullname'] = $data['fullname'];
		$data_booking['id_card_number'] = $data['id_card_number'];
		$data_booking['seller'] = $data['seller'];
		$data_booking['created_by'] = 1;
		$data_booking['created_on'] = date('Y-m-d H:i:s');

		$id = $this->booking_model->insert($data_booking);
		$booking_service_id = 0;

		// Get list service price to re-calculate total bill before saving to db
		$list_services = $this->services_model->get_list_service_price();

		if (is_numeric($id)) {
			// Save services in this booking
			$total_bill = 0;
			foreach ($data['services'] as $service) {
				// Generate code for service
				$price = $list_services[$service['service']];
				$data_service = [];
				$data_service['service_code'] = $this->generateRandomString();
				$data_service['booking_id'] = $id;
				$data_service['service_id'] = $service['service'];
				$data_service['vinpearl_id'] = $service['vinpearl_id'];
				$data_service['price'] = $price;
				$data_service['quanlity'] = $service['quanlity'];

				$booking_service_id = $this->booking_service_model->insert($data_service);
				$total_bill += intval($price) * intval($service['quanlity']);
			}
	
			if (!is_numeric($booking_service_id)) {
				$response['message'] = 'Lỗi khi thêm dịch vụ. Vui lòng thử lại sau.';
				echo json_encode($response); die; 
			}

			$updated = $this->booking_model->skip_validation(true)->update($id, ['total_bill' => $total_bill]);;
		} else {
			$response['message'] = 'Thêm dịch vụ lỗi, hãy kiểm tra lại thông tin';
			echo json_encode($response); die; 
		}
		
		$response = [
			'type' => 'success',
			'message' => 'Thêm dịch vụ thành công cho Mã booking "' . $data['booking_code'] . '"',
			'redirect_url' => base_url('home/check_booking/' . $id)
		];
		echo json_encode($response); die; 
	}

	public function check_booking($id = '') {
		$booking_id = $this->uri->segment(3);
		if (!$booking_id) {
			Template::set_message('Không có mã đặt chỗ trên hệ thống.', 'error');
			show_404();
		}

		// Get booking by Id
		$this->load->model('booking_model');
		$booking_details = $this->booking_model->get_list_booking_with_services($id);

		Assets::add_js('codeigniter-csrf.js');
		Assets::add_js('vendors/bootstrap-growl/bootstrap-growl.min.js');
		Assets::add_js('vendors/QR_code/jquery-qrcode-0.17.0.min.js');
		Assets::add_js('themes/default/js/home.js');
		Assets::add_css('themes/default/css/home.css');
		Template::set('booking_details', $booking_details);
		Template::render();
	}

	public function check_service_code($service_code = '') {
		$service_code = $this->uri->segment(3) ? 
						$this->uri->segment(3) : 
						$_GET['service_code'];
		if (!$service_code) {
			Template::set_message('Không có mã vé trên hệ thống.', 'error');
			show_404();
		}

		// Get booking by Id
		$this->load->model('booking_service_model');
		$service_details = $this->booking_service_model->get_service_detail($service_code);
		if (!$service_details) {
			Template::set_message('Không có mã vé trên hệ thống.', 'error');
			redirect('/');
		}

		Assets::add_js('codeigniter-csrf.js');
		Assets::add_js('vendors/bootstrap-growl/bootstrap-growl.min.js');
		Assets::add_js('vendors/QR_code/jquery-qrcode-0.17.0.min.js');
		Assets::add_js('themes/default/js/home.js');
		Assets::add_css('themes/default/css/home.css');
		Template::set('service_details', $service_details[0]);
		Template::render();
	}

	public function get_list_service_by_vinpearl() {
		$response = [
			'type' => 'error',
			'message' => 'submit error'
		];

		$vinpearl_id = $this->input->post('vinpearl_id');
		// Get list service
		$list_service_by_vinpearl = $this->db
							->select('s.id as service_id, s.price, 
							s.service_name, v.vinpearl, v.code, v.id as vinpearl_id')
							->from('vinpearl_services vs')
							->join('vinpearl v', 'v.id = vs.vinpearl_id')
							->join('services s', 's.id = vs.service_id')
							->where('vs.vinpearl_id', $vinpearl_id)
							->get()->result();

		if ($list_service_by_vinpearl) {
			$response = [
				'type' => 'success',
				'message' => 'get list success',
				'list_service' => json_encode($list_service_by_vinpearl)
			];
		}
		echo json_encode($response); die; 
	}
	//--------------------------------------------------------------------

	/**
	 * If the Auth lib is loaded, it will set the current user, since users
	 * will never be needed if the Auth library is not loaded. By not requiring
	 * this to be executed and loaded for every command, we can speed up calls
	 * that don't need users at all, or rely on a different type of auth, like
	 * an API or cronjob.
	 *
	 * Copied from Base_Controller
	 */
	protected function set_current_user()
	{
        if (class_exists('Auth')) {
			// Load our current logged in user for convenience
            if ($this->auth->is_logged_in()) {
				$this->current_user = clone $this->auth->user();

				$this->current_user->user_img = gravatar_link($this->current_user->email, 22, $this->current_user->email, "{$this->current_user->email} Profile");

				// if the user has a language setting then use it
                if (isset($this->current_user->language)) {
					$this->config->set_item('language', $this->current_user->language);
				}
            } else {
				$this->current_user = null;
			}

			// Make the current user available in the views
            if (! class_exists('Template')) {
				$this->load->library('Template');
			}
			Template::set('current_user', $this->current_user);
		}
	}

	function generateRandomString($length = 15) {
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
/* end ./application/controllers/home.php */
