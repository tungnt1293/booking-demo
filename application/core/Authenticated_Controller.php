<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Authenticated Controller
 *
 * Provides a base class for all controllers that must check user login status.
 *
 * @package  Bonfire\Core\Controllers\Authenticated_Controller
 * @category Controllers
 * @author   Bonfire Dev Team
 * @link     http://cibonfire.com/docs
 *
 */
class Authenticated_Controller extends Base_Controller
{

    protected $require_authentication = true;

    //--------------------------------------------------------------------------

    /**
     * Class constructor setup login restriction and load various libraries
     *
     * @return void
     */
    public function __construct()
    {
        $this->autoload['helpers'][]   = 'form';
        $this->autoload['libraries'][] = 'Template';
        $this->autoload['libraries'][] = 'Assets';
        $this->autoload['libraries'][] = 'form_validation';

        parent::__construct();

        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters('', '');
        Assets::add_js([
            'vendors/bower_components/flot/jquery.flot.js',
            'vendors/bower_components/flot/jquery.flot.resize.j',
            'vendors/bower_components/flot/jquery.flot.pie.js',
            'vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js',
        ]);
        Assets::add_js('summary.js');
        $this->getDueDate();
    }

    protected function getDueDate() {
        $this->load->model('event/event_model');
        Template::set('due_date_today', $this->event_model->get_events_due_date());
        Template::set('due_date_within7days', $this->event_model->get_events_due_date(1));
        Template::set('static_event', json_encode($this->event_model->get_number_task_assigned_each_person()));
    }
}
