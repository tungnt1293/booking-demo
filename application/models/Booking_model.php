<?php defined('BASEPATH') || exit('No direct script access allowed');

class Booking_model extends BF_Model
{
	protected $table_name	= 'booking';
	protected $key			= 'id';
	protected $date_format	= 'datetime';

	protected $log_user 	= false;
	protected $set_created	= true;
	protected $set_modified = false;
	protected $soft_deletes	= false;

	protected $created_field     = 'created_on';
	protected $created_by_field  = 'created_by';
	protected $modified_field    = 'modified_on';
	protected $modified_by_field = 'modified_by';
	protected $deleted_field     = 'deleted';
	protected $deleted_by_field  = 'deleted_by';

	// Customize the operations of the model without recreating the insert,
	// update, etc. methods by adding the method names to act as callbacks here.
	protected $before_insert 	= array();
	protected $after_insert 	= array();
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 	    = array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	// For performance reasons, you may require your model to NOT return the id
	// of the last inserted row as it is a bit of a slow method. This is
	// primarily helpful when running big loops over data.
	protected $return_insert_id = true;

	// The default type for returned row data.
	protected $return_type = 'object';

	// Items that are always removed from data prior to inserts or updates.
	protected $protected_attributes = array();

	// You may need to move certain rules (like required) into the
	// $insert_validation_rules array and out of the standard validation array.
	// That way it is only required during inserts, not updates which may only
	// be updating a portion of the data.
	protected $validation_rules 		= array(
		array(
			'field' => 'booking_code',
			'label' => 'lang:field_booking_code',
			'rules' => 'required|unique[bf_booking.booking_code]|trim|max_length[25]',
		),
		array(
			'field' => 'fullname',
			'label' => 'lang:field_fullname',
			'rules' => 'required|trim|max_length[255]',
		),
		array(
			'field' => 'id_card_number',
			'label' => 'lang:field_short_id_card_number',
			'rules' => 'required|is_numeric|trim|max_length[12]',
		)
	);
	protected $insert_validation_rules  = array();
	protected $skip_validation 			= false;

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
    /**
	 * Return
	 *
	*/
	public function get_list_booking_with_services($id) {
		$booking_detail = $this->db
						->select('b.id, b.booking_code ,b.fullname as customer, b.id_card_number, 
						b.booking_date, b.total_bill, e.fullname as seller')
						->from($this->table_name . ' b')
						->join('employees e', 'b.seller = e.id')
						->where('b.id', $id)
						->get()->result();

		// Get list service 
		$booking_detail['services'] = [];
		$booking_services = $this->db
							->select('bs.service_code, bs.booking_id, bs.service_id, 
							bs.quanlity, bs.price, s.service_name')
							->from('booking_service bs')
							->join('services s', 's.id = bs.service_id')
							->where('bs.booking_id', $id)
							->get()->result();
		foreach($booking_services as $bs) {
			array_push($booking_detail['services'], $bs);
		}

		return $booking_detail;
	}
}