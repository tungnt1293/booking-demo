<?php defined('BASEPATH') || exit('No direct script access allowed');

class Booking_service_model extends BF_Model
{
	protected $table_name	= 'booking_service';
	protected $key			= 'id';
	protected $date_format	= 'datetime';

	protected $log_user 	= false;
	protected $set_created	= false;

	public function get_service_detail($service_code) {
		$service_detail = $this->db
						->select('bs.service_code, bs.booking_id, bs.service_id, bs.vinpearl_id, p.vinpearl,
						bs.quanlity, bs.price, b.booking_code, b.booking_date, b.fullname, s.service_name')
						->from($this->table_name . ' bs')
						->join('booking b', 'b.id = bs.booking_id')
						->join('services s', 's.id = bs.service_id')
						->join('vinpearl p', 'p.id = bs.vinpearl_id')
						->where('bs.service_code', $service_code)
						->get()->result();
		return $service_detail;
	}
}