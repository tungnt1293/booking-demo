<?php defined('BASEPATH') || exit('No direct script access allowed');

class Employees_model extends BF_Model
{
	protected $table_name	= 'employees';
	protected $key			= 'id';
	protected $date_format	= 'datetime';

	protected $log_user 	= false;
	protected $set_created	= true;
}