<?php defined('BASEPATH') || exit('No direct script access allowed');

class Services_model extends BF_Model
{
	protected $table_name	= 'services';
	protected $key			= 'id';
	protected $date_format	= 'datetime';

	protected $log_user 	= false;
	protected $set_created	= false;

	public function get_list_service_price() {
		$list_service = $this->db
						->from($this->table_name)
						->select('id, price')->get()->result();
		$temp = [];
		foreach ($list_service as $le) {
			$temp[$le->id] = $le->price;
		}
		return $temp;
	}

	// public function get_list_service_by_vinpearl() {
	// 	$list_services = $this->db
	// 					->select('s.id as service_id, s.price, s.service_name, v.vinpearl, v.code')
	// 					->from('vinpearl_services vs')
	// 					->join('vinpearl v', 'v.id = vs.vinpearl_id')
	// 					->join('services s', 's.id = vs.service_id')
	// 					->get()->result();
	// 	$temp = [];
	// 	foreach ($list_services as $list_service) {
	// 		$temp[$list_service->code] = isset($temp[$list_service->code]) ? $temp[$list_service->code] : [];
	// 		$temp[$list_service->code]['services'] = isset($temp[$list_service->code]['services']) ? 
	// 												$temp[$list_service->code]['services']: [];
	// 		$temp[$list_service->code]['services'][$list_service->service_id]['name'] = $list_service[]
	// 	}
	// 	dump($list_services);
	// }
}