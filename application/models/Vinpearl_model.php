<?php defined('BASEPATH') || exit('No direct script access allowed');

class Vinpearl_model extends BF_Model
{
	protected $table_name	= 'vinpearl';
	protected $key			= 'id';
	protected $date_format	= 'datetime';

	protected $log_user 	= false;
	protected $set_created	= false;

}