<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['module_config'] = array(
        'name'        => 'lang:nav_email_templates_name',
        'description' => 'lang:nav_email_templates_desc',
        'author'      => 'SGS Web Dev Team',
        'version'     => '1',
);


$config['template_type'] = array(
    'TEXT' => 'Text',
    'HTML' => 'Html'
);