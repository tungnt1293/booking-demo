<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class settings extends Admin_Controller {

    //--------------------------------------------------------------------


    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Email_Templates.Settings.View');
        $this->load->model('email_templates_model', null, true);
        $this->lang->load('email_templates');
        $this->load->config('config');

        Assets::add_module_css('email_templates', 'email_templates.css');
        Assets::add_js('vendors/ckeditor/ckeditor.js');
        Assets::add_js('vendors/bower_components/chosen/chosen.jquery.js');
        Assets::add_css('vendors/bower_components/chosen/chosen.css');
        Assets::add_js($this->load->view('settings/js', null, true), 'inline');
        Template::set_block('sub_nav', 'settings/_sub_nav');
    }

    //--------------------------------------------------------------------



    /*
      Method: index()

      Displays a list of form data.
     */
    public function index() {

        $limit = $this->settings_lib->item('site.list_limit');
        $offset = $this->input->get('per_page');
        $language = $this->email_templates_model->get_language_default();

        // Pagination
        $this->load->library('pagination');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['total_rows'] = $this->email_templates_model->where('language_code',$language)->count_all();
        $this->pager['per_page'] = $limit;
        $this->pager['page_query_string'] = true;
        $this->pagination->initialize($this->pager);

        $records = $this->email_templates_model->where('language_code',$language)->limit($limit, $offset)->order_by('email_template_key')->find_all();

        Template::set('records', $records);
        Template::set('toolbar_title', lang('email_templates_manage'));
        Template::render();
    }

    //--------------------------------------------------------------------



    /*
      Method: edit()

      Allows editing of Email Templates data.
     */
    public function edit() {
        $this->auth->restrict('Email_Templates.Settings.Edit');

        $id = $this->uri->segment(5);
        $languages = $this->email_templates_model->get_languages();
        $language = $this->input->get('language');

        if (!isset($language) || $language == '') {
            $language = $this->email_templates_model->get_language_default();
        }

        if (empty($id)) {
            Template::set_message(lang('email_templates_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/email_templates');
        }

        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('email_template_description', 'lang:email_template_description', 'max_length[255]');

            if ($this->form_validation->run() === FALSE) {
                Template::set_message(lang('email_templates_edit_failure') . validation_errors(), 'error');
            } else if ($this->save_email_templates($id)) {
                Template::set_message(lang('email_templates_edit_success'), 'success');
                //redirect(SITE_AREA . '/settings/email_templates');
            } else {
                Template::set_message(lang('email_templates_edit_failure') . $this->email_templates_model->error, 'error');
            }
        }

        Template::set('language', $language);
        Template::set('languages', $languages);
        Template::set('email_templates', $this->email_templates_model->where('language_code', $language)->find($id));
        Template::set('toolbar_title', lang('email_templates_edit') . ' ' . lang('email_template'));
        Template::render();
    }

    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------
    private function save_email_templates($id = 0) {
        $data = $this->email_templates_model->prep_data($this->input->post());
        $data['language_code'] = $this->input->post('language_code');

        if ($data['template_type'] == "HTML")
            $data['email_template_content'] = htmlspecialchars($data['email_template_content']);

        $exits = $this->email_templates_model->count_by(array('email_template_key' => $id, 'language_code' => $data['language_code']));

        if ($exits) {
            $return = $this->email_templates_model->where('language_code', $data['language_code'])->update_where('email_template_key', $id, $data);
        } else {
            $data['email_template_key'] = $id;
            $this->email_templates_model->insert($data);
            $return = $this->email_templates_model->db->affected_rows();
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
