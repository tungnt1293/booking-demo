<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['email_templates'] = 'Email templates';
$lang['email_templates_manage'] = 'Manage Email Templates';
$lang['email_templates_edit'] = 'Edit';
$lang['email_templates_true'] = 'True';
$lang['email_templates_false'] = 'False';
$lang['email_templates_create'] = 'Create';
$lang['email_templates_list'] = 'List';
$lang['email_templates_new'] = 'New';
$lang['email_templates_edit_text'] = 'Edit this to suit your needs';
$lang['email_templates_no_records'] = 'There aren\'t any email_templates in the system.';
$lang['email_templates_create_new'] = 'Create a new Email Templates.';
$lang['email_templates_create_success'] = 'Email Templates successfully created.';
$lang['email_templates_create_failure'] = 'There was a problem creating the email_templates: ';
$lang['email_templates_create_new_button'] = 'Create New Email Templates';
$lang['email_templates_invalid_id'] = 'Invalid Email Templates ID.';
$lang['email_templates_edit_success'] = 'Email Templates successfully saved.';
$lang['email_templates_edit_failure'] = 'There was a problem saving the email_templates: ';
$lang['email_templates_delete_success'] = 'record(s) successfully deleted.';
$lang['email_templates_delete_failure'] = 'We could not delete the record: ';
$lang['email_templates_delete_error'] = 'You have not selected any records to delete.';
$lang['email_templates_actions'] = 'Actions';
$lang['email_templates_cancel'] = 'Cancel';
$lang['email_templates_delete_record'] = 'Delete this Email Templates';
$lang['email_templates_delete_confirm'] = 'Are you sure you want to delete this email_templates?';
$lang['email_templates_edit_heading'] = 'Edit Email Templates';

// Activities
$lang['email_templates_act_create_record'] = 'Created record with ID';
$lang['email_templates_act_edit_record'] = 'Updated record with ID';
$lang['email_templates_act_delete_record'] = 'Deleted record with ID';

$lang['email_template'] = 'Email template';
$lang['email_template_key'] = 'Key';
$lang['email_template_description'] = 'Template description';
$lang['email_template_content'] = 'Email content';
$lang['email_template_type'] = 'Template type';
$lang['email_template_modified_on'] = 'Modified on';
$lang['email_template_no_record'] = 'No records found that match your selection.';
$lang['email_template_fix_error'] = 'Please fix the following errors :';
$lang['email_templates_edit_button'] = 'Update';
$lang['email_title'] = 'Email title';

$lang['select_language'] = 'Select language';
$lang['language'] = 'Language';
