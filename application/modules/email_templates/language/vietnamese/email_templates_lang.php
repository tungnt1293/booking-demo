<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['email_templates_manage'] = 'Các mẫu email';
$lang['email_templates_edit'] = 'Sửa';
$lang['email_templates_true'] = 'Đúng';
$lang['email_templates_false'] = 'Sai';
$lang['email_templates_create'] = 'Tạo mới';
$lang['email_templates_list'] = 'Danh sách';
$lang['email_templates_new'] = 'Thêm mới';
$lang['email_templates_edit_text'] = 'Chỉnh sửa cho phù hợp với bạn';
$lang['email_templates_no_records'] = 'Không có mẫu email nào trong hệ thống';
$lang['email_templates_create_new'] = 'Tạo mới mẫu email';
$lang['email_templates_create_success'] = 'Tạo thành công mẫu email mới';
$lang['email_templates_create_failure'] = 'Có lỗi khi tạo mới mẫu email: ';
$lang['email_templates_create_new_button'] = 'Tạo mới mẫu email';
$lang['email_templates_invalid_id'] = 'Sai ID của email mẫu.';
$lang['email_templates_edit_success'] = 'Lưu mẫu email thành công';
$lang['email_templates_edit_failure'] = 'Có lỗi xảy ra khi lưu mẫu email: ';
$lang['email_templates_delete_success'] = 'bản ghi đã được xóa.';
$lang['email_templates_delete_failure'] = 'Không thể xóa bản ghi: ';
$lang['email_templates_delete_error'] = 'Bạn chưa chọn bản ghi để xóa.';
$lang['email_templates_actions'] = 'Hành động';
$lang['email_templates_cancel'] = 'Hủy';
$lang['email_templates_delete_record'] = 'Xóa mẫu email này';
$lang['email_templates_delete_confirm'] = 'Bạn muốn xóa mẫu email này?';
$lang['email_templates_edit_heading'] = 'Chỉnh sửa mẫu email';
$lang['email_templates_act_create_record'] = 'Tạo bản ghi với ID';
$lang['email_templates_act_edit_record'] = 'Cập nhật bản ghi với ID';
$lang['email_templates_act_delete_record'] = 'Xóa bản ghi với ID';

$lang['email_template_key'] = 'Key';
$lang['email_template_description'] = 'Mô tả';
$lang['email_template_content'] = 'Nội dung';
$lang['email_template_type'] = 'Kiểu email';
$lang['email_template_modified_on'] = 'Lần sửa cuối';
$lang['email_template_no_record'] = 'Không tìm thấy bản ghi nào phù hợp với lựa chọn của bạn.';
$lang['email_template_fix_error'] = 'Sửa những lỗi dưới đây :';
$lang['email_templates_edit_button'] = 'Cập nhật';
$lang['email_title'] = 'Tiêu đề email';