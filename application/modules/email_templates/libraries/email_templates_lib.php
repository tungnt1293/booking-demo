<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_templates_lib {
    private $ci;
    public $default_language;

    public function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->model('email_templates/email_templates_model');
        $this->ci->load->library('emailer/emailer');
        $this->ci->load->library('parser');
    }

    public function get_email_template($key, $language, array $replace_content) {
        $template = $this->ci->email_templates_model->find_by(array('email_template_key' => $key, 'language_code' => $language));

        if ($template) {
            $result = array();
            $result['subject'] = $this->ci->parser->parse_string($template->email_title, $replace_content, TRUE); // Replace content for title
            $result['template_type'] = $template->template_type;
            $template = html_entity_decode($template->email_template_content);
            $template = $this->ci->parser->parse_string($template, $replace_content, TRUE);
            $result['template_content'] = $template;

            return $result;
        }
        return FALSE;
    }

    public function send_email($to, $key, $language, array $replace_content, $queue_email = TRUE) {
        if (!$this->default_language) {
            $this->default_language = $this->ci->email_templates_model->get_language_default();
        }

        $mail_settings = $this->get_email_template($key, $language, $replace_content);
        if (!$mail_settings) {
            $mail_settings = $this->get_email_template($key, $this->default_language, $replace_content);
        }

        $data = [
            'to' => $to,
            'subject' => $mail_settings['subject'],
            'message' => $mail_settings['template_content']
        ];
        $this->ci->emailer->send($data, $queue_email);
    }
}
