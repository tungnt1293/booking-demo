<ul class="nav nav-pills">
    <li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
        <a href="<?php echo site_url(SITE_AREA . '/settings/email_templates') ?>"><?php echo lang('email_templates_list'); ?></a>
    </li>
</ul>