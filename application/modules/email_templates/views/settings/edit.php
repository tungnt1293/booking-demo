<?php if (validation_errors()) : ?>
    <div class="alert alert-block alert-error fade in ">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading"><?php echo lang('email_template_fix_error'); ?></h4>
        <?php echo validation_errors(); ?>
    </div>
<?php endif; ?>
<?php
if (isset($email_templates)) {
    $email_templates = (array) $email_templates;
}
$id = isset($email_templates['email_template_key']) ? $email_templates['email_template_key'] : '';
$language = $this->input->get('language') !== null && $this->input->get('language') !== '' ? $this->input->get('language') : $language;
?>
<div class="admin-box">
    <?php echo form_open($this->uri->uri_string() . '?language=' . set_value('language', $language), 'class="form-horizontal"'); ?>
    <fieldset>
    	<legend><?php e(lang('select_language')) ?></legend>
    	<?php echo form_dropdown('language_code', $languages, set_value('language', $language), lang('language')); ?>
    </fieldset>

    <fieldset>
    	<legend><?php e(lang('email_template')) ?></legend>
        <div class="control-group <?php echo form_error('email_template_description') ? 'error' : ''; ?>">
            <?php echo form_label(lang('email_template_description'), 'email_template_description', array('class' => "control-label")); ?>
            <div class='controls'>
                <?php echo form_textarea(array('name' => 'email_template_description', 'id' => 'email_template_description', 'class' => 'span6', 'rows' => '6', 'cols' => '80', 'value' => set_value('email_template_description', isset($email_templates['email_template_description']) ? $email_templates['email_template_description'] : ''))) ?>
                <span class="help-inline"><?php echo form_error('email_template_description'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('email_title') ? 'error' : ''; ?>">
            <?php echo form_label(lang('email_title'), 'email_title', array('class' => "control-label")); ?>
            <div class='controls'>
                <input id="email_title" class="span6" type="text" name="email_title" maxlength="255" value="<?php echo set_value('email_title', isset($email_templates['email_title']) ? $email_templates['email_title'] : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('email_title'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('email_template_content') ? 'error' : ''; ?>">
            <?php echo form_label(lang('email_template_content'), 'email_template_content', array('class' => "control-label")); ?>
            <div class='controls'>
                <textarea name="email_template_content" id="email_template_content">
                	<?php echo set_value('email_template_content', isset($email_templates['email_template_content']) ? htmlspecialchars_decode($email_templates['email_template_content']) : '') ?>
				</textarea>
                <span class="help-inline"><?php echo form_error('email_template_content'); ?></span>
            </div>
        </div>

        <?php echo form_dropdown(array('name' => 'template_type', 'id' => 'template_type'), $this->config->item('template_type'), set_value('template_type', isset($email_templates['template_type']) ? $email_templates['template_type'] : ''), lang('email_template_type')) ?>

        <div class="form-actions">
            <button type="submit" name="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> <?php echo lang('email_templates_edit_button'); ?></button>
            <?php echo anchor(SITE_AREA . '/settings/email_templates', '<i class="icon-remove icon-white"></i> ' . lang('email_templates_cancel'), 'class="btn btn-warning"'); ?>
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>
