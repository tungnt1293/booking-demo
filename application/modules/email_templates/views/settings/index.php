<?php 
$this->load->helper('text');
?>

<div class="admin-box">
    <h3><?php e(lang('email_templates'))?></h3>
    <?php echo form_open($this->uri->uri_string()); ?>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <?php if ($this->auth->has_permission('Email_Templates.Settings.Delete') && isset($records) && is_array($records) && count($records)) : ?>
                    <th class="column-check"><input class="check-all" type="checkbox" /></th>
                <?php endif; ?>

                <th><?php echo lang('email_template_key'); ?></th>
                <th><?php echo lang('email_template_description'); ?></th>
                <th><?php echo lang('email_template_type'); ?></th>
                <th><?php echo lang('email_template_modified_on'); ?></th>
            </tr>
        </thead>
        <?php if (isset($records) && is_array($records) && count($records)) : ?>
            <tfoot>
                <?php if ($this->auth->has_permission('Email_Templates.Settings.Delete')) : ?>
                    <tr>
                        <td colspan="6">
                            <?php echo lang('bf_with_selected') ?>
                            <input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete') ?>" onclick="return confirm('<?php echo lang('email_templates_delete_confirm'); ?>')">
                        </td>
                    </tr>
                <?php endif; ?>
            </tfoot>
        <?php endif; ?>
        <tbody>
            <?php if (isset($records) && is_array($records) && count($records)) : ?>
                <?php foreach ($records as $record) : ?>
                    <tr>
                        <?php if ($this->auth->has_permission('Email_Templates.Settings.Delete')) : ?>
                            <td><input type="checkbox" name="checked[]" value="<?php echo $record->email_template_key ?>" /></td>
                        <?php endif; ?>

                        <?php if ($this->auth->has_permission('Email_Templates.Settings.Edit')) : ?>
                            <td><?php echo anchor(SITE_AREA . '/settings/email_templates/edit/' . $record->email_template_key, '<i class="icon-pencil">&nbsp;</i>' . $record->email_template_key) ?></td>
                        <?php else: ?>
                            <td><?php echo $record->email_template_key ?></td>
                        <?php endif; ?>

                        <td><?php echo word_limiter($record->email_template_description, 15) ?></td>
                        <td><?php echo $record->template_type ?></td>
                        <td><?php echo $record->modified_on ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="6"><?php echo lang('email_template_no_record'); ?></td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
    <?php echo form_close(); ?>
    <br /><br />
    <?php echo $this->pagination->create_links(); ?>
</div>