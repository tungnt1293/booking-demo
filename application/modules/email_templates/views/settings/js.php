CKEDITOR.replace( 'email_template_content', {
    fullPage: false,
    allowedContent: true,
    height: 500,
    enterMode : CKEDITOR.ENTER_BR,
});


$('#language_code').change(function (){
	window.location = '<?php echo base_url($this->uri->uri_string()) ?>?language=' + $(this).val();
});
