<?php 
    $booking_info = $booking_details[0];
    $booking_services = $booking_details['services'];
?>
<div class="card booking-detail">
    <div class="card-header bgm-bluegray">
        <h2>Thông tin dịch vụ theo mã booking</h2>
    </div>
    <div class="card-body card-padding">
        <div class="booking-info">
            <table class="table table-striped table-hover">
                <tbody>
                    <tr>
                        <th class="col-5">Mã Booking</th>
                        <td> 
                            <a href="<?php echo base_url('home/check_booking/' . $booking_info->id);?>">
                                <?php echo $booking_info->booking_code; ?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th class="col-5">Tên khách hành</th>
                        <td><?php echo $booking_info->customer; ?></td>
                    </tr>
                    <tr>
                        <th class="col-5">Chứng minh thư</th>
                        <td><?php echo $booking_info->id_card_number; ?></td>
                    </tr>
                    <tr>
                        <th class="col-5">Ngày đặt</th>
                        <td><?php echo $booking_info->booking_date; ?></td>
                    </tr>
                    <tr>
                        <th class="col-5">Nhân viên bán hàng</th>
                        <td><?php echo $booking_info->seller; ?></td>
                    </tr>
                    <tr>
                        <th class="col-5">Giá trị đơn hàng</th>
                        <td><?php echo number_format($booking_info->total_bill); ?> đ</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        <div class="booking-services">
            <div style="text-align:center;">
                <h3>Danh sách dịch vụ</h3>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Mã vé</th>
                        <th>Tên dịch vụ</th>
                        <th>Giá tiền</th>
                        <th>Số lượng</th>
                        <th>Thành tiền</th>
                        <th>Xem vé</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($booking_services as $bs): ?>
                        <tr>
                            <th><?php echo $bs->service_code; ?></th>
                            <td><?php echo $bs->service_name; ?></td>
                            <td><?php echo number_format($bs->price); ?> đ</td>
                            <td><?php echo $bs->quanlity; ?></td>
                            <td><?php echo number_format($bs->price * $bs->quanlity); ?> đ</td>
                            <td>
                                <a href="<?php echo base_url('home/check_service_code/' . $bs->service_code); ?>"
                                class="btn btn-default" target="_blank">Xem vé</a>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>