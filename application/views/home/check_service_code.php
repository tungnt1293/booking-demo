<div class="" style="display: flex;justify-content: center;">    
    <div class="card card-service-ticket col-md-6">
        <div class="card-header" style="text-align:center;">
            <h2><b><?php echo $service_details->vinpearl; ?></b> - <?php echo $service_details->service_name; ?></h2>
            <small style="color:blue;">Đọc mã vé hoặc quét QR code để sử dụng dịch vụ</small>
        </div>
        <div class="card-body card-padding">
            <div class="qr-code">
            </div>
            <hr>
            <div class="more-info">
                <table class="table table-striped table-hover">
                    <tbody>
                        <tr>
                            <th class="col-3">Mã vé</th>
                            <td><?php echo $service_details->service_code; ?></td>
                        </tr>
                        <tr>
                            <th class="col-3">Khách hàng</th>
                            <td><?php echo $service_details->fullname; ?></td>
                        </tr>
                        <tr>
                            <th class="col-3">Ngày đặt</th>
                            <td><?php echo $service_details->booking_date; ?></td>
                        </tr>
                        <tr>
                            <th class="col-3">Đơn giá</th>
                            <td><?php echo number_format($service_details->price); ?> đ</td>
                        </tr>
                        <tr>
                            <th class="col-3">Số lượng</th>
                            <td><?php echo $service_details->quanlity; ?></td>
                        </tr>
                        <tr>
                            <th class="col-3">Thành tiền</th>
                            <td><?php echo number_format($service_details->price * $service_details->quanlity); ?> đ</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var qr_text = '<?php echo $service_details->service_code; ?>';
</script>