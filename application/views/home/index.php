<?php 
	$booking_code = isset($_POST['booking_code']) ? $_POST['booking_code'] : '';
	$fullname = isset($_POST['fullname']) ? $_POST['fullname'] : '';
	$booking_date = isset($_POST['booking_date']) ? $_POST['booking_date'] : date('Y-m-d');
	$id_card_number = isset($_POST['id_card_number']) ? $_POST['id_card_number'] : '';
	$seller = isset($_POST['seller']) ? $_POST['seller'] : '';
	$service = isset($_POST['service']) ? $_POST['service'] : '';
	$quantity = isset($_POST['quantity']) ? $_POST['quantity'] : 1;
	$total_bill = isset($_POST['total_bill']) ? $_POST['total_bill'] : '';
?>
<?php echo form_open(current_url(), 'class="form-horizontal my-form"'); ?>
	<div class="col-md-5">
		<div class="card card-customer-info ">
			<div class="card-header bgm-bluegray">
				<h2><i class="zmdi zmdi-account-o zmdi-2x"></i>  <?php echo lang('client_info'); ?></h2>
				<small><?php echo lang('note_for_title'); ?></small>
			</div>
			<div class="card-body card-padding min-500-max-500">
				<!-- Booking Code -->
				<?php echo form_input(['name' => 'booking_code', 'class' => 'form-control booking-code required'], 
				$booking_code, lang('booking_code'),  'placeholder="Nhập mã booking không quá 25 ký tự, gồm chữ và số"'); ?>
				<!-- Customer Fullname -->
				<?php echo form_input(['name' => 'fullname', 'class' => 'form-control fullname required'], 
				$fullname, lang('fullname'), 'placeholder="Tên người mua không quá 200 ký tự"'); ?>
				<!-- ID Card -->
				<?php echo form_input(['name' => 'id_card_number', 'class' => 'form-control id-card required'], 
				$id_card_number, lang('id_card_number'), 'placeholder="Chứng minh thư phải là số, không quá 12 ký tự"'); ?>
				<!-- Booking Date -->
				<?php echo form_date(['name' => 'booking_date', 'class' => 'form-control booking-date required'], 
				$booking_date, lang('booking_date'), ''); ?>
				<!-- Seller -->
				<div class="form-group fg-line">
					<label for="seller"><?php echo  lang('seller');?></label>
					<select name="seller" class="form-control seller required">
						<option value="0" data-price="0"><?php echo  lang('choose_employee');?></option>
						<?php foreach($list_employees as $le) :?>
							<option value="<?php echo $le->id; ?>"
							<?php echo $seller == $le->id ? 'selected' : ''; ?>>
								<?php echo $le->fullname . ' (' . $le->employee_code . ')'; ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7">
		<div class="card card-service">
			<div class="card-header bgm-bluegray">
				<h2><i class="zmdi zmdi-bookmark-outline zmdi-2x"></i>  <?php echo lang('service_info'); ?></h2>
				<small><?php echo lang('note_for_service_info'); ?></small>
			</div>
			<div class="card-body card-padding min-500-max-500" style="padding: 26px 26px;">
				<div style="min-height: 50px;">
					<!-- Service total  -->
					<b><?php echo lang('service_counting'); ?></b> : <span class="service-counting-total">0</span> |
					<!-- Total Price -->
					<label for="total-bill"><b><?php echo  lang('total_bill');?> : </b></label>
					<span class="total-bill">0</span> đ
					<button class="add-service btn btn-warning c-white">
						<?php echo lang('add_service'); ?>
					</button>
				</div>
				<div class="services-wrapper" style="max-height:350px;overflow-y: scroll;margin-bottom:10px;">
					<!-- List Services -->
				</div>
				<div class="submit-wrapper" style="text-align:right;">
					<button type="submit" name="submit" class="btn btn-warning">
						<?php echo lang('submit_button'); ?>
					</button>
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>

<div class="template" style="display:none;">
	<div id="template_service">
		<div class="service-wrapper row">
			<div class="col-md-1 lh-60">
				<label class="service-counting">{0}</label>
			</div>
			<div class="col-md-3">
				<div class="form-group fg-line">
					<label for="vinpearl"><?php echo lang('list_vinpearl');?></label>
					<select name="services[{0}][vinpearl_id]" class="form-control vinpearl-select required">
						<option value="0" data-price="0"><?php echo lang('choose_vinpearl');?></option>
						<?php foreach($list_vinpearl as $lp) :?>
							<option value="<?php echo $lp->id; ?>" data-code="<?php echo $lp->code; ?>"
							<?php echo $service == $lp->id ? 'selected' : ''; ?>>
								<?php echo $lp->vinpearl; ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>	
			<div class="col-md-4">
				<div class="form-group fg-line">
					<label for="services"><?php echo lang('list_services');?></label>
					<select name="services[{0}][service]" class="form-control service-select required">
						<option value="0" data-price="0"><?php echo lang('choose_services');?></option>

					</select>
				</div>
			</div>	
			<!-- Price -->
			<div class="col-md-2">
				<div class="form-group fg-line">
					<label for="price"><?php echo lang('price'); ?></label>
					<input name="services[{0}][price]" type="text" value="0" readonly
					class="form-control price text-center" id="price">
				</div>
			</div>
			<!-- Quanlity -->
			<div class="col-md-1">
				<div class="form-group fg-line">
					<label for="quanlity"><?php echo lang('quanlity'); ?></label>
					<input name="services[{0}][quanlity]" type="number" value="1" min="1" step="1"
					class="form-control text-center quanlity" id="quanlity">
				</div>
			</div>
			<!-- Action -->
			<div class="col-md-1" style='line-height: 65px;'>
				<button class="btn btn-danger remove-service">x</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"> 
	var list_services = <?php echo json_encode($list_services); ?>;
	var submit_url = '<?php echo base_url('home/submit_booking'); ?>';
	var get_list_service_url = '<?php echo base_url('home/get_list_service_by_vinpearl'); ?>';
	var ci_csrf_token_name = '<?php echo $this->security->get_csrf_token_name(); ?>';
</script>