$(document).ready(function() {
  $("#event_chart")[0] && $.plot("#event_chart", JSON.parse(static_event), {
    series: {
      pie: {
        innerRadius: .5,
        show: !0,
        stroke: {
          width: 1
        }
      }
    },
    legend: {
      container: ".flc-donut",
      backgroundOpacity: .5,
      noColumns: static_event.count,
      backgroundColor: "white",
      lineWidth: 0,
    },
    grid: {
      hoverable: !0,
      clickable: !0
    },
    tooltip: !0,
    tooltipOpts: {
      content: "%y.0, %s",
      shifts: {
        x: 20,
        y: 0
      },
      defaultTheme: !1,
      cssClass: "flot-tooltip"
    }
  });
});

$('.my_due_task').on('click', function() {
  // const user_id = $('#current_user_id').val();
  if (!$('#ts1').is(':checked')) {
      $('.due_task').each(function() {
          if ($(this).attr('assignee') !== user_current_id) {
              $(this).hide();
          }
      });
  } else {
      $('.due_task').each(function() {
          $(this).show();
      });
  }
  
});