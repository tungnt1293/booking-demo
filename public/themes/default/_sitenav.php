<header id="header" class="clearfix" data-ma-theme="bluegray" style="padding-bottom: 10px;"> 
	<ul class="h-inner">
		<li class="hi-logo col-md-5">
			<a href="<?php echo site_url(); ?>">
				<?php e(class_exists('Settings_lib') ? settings_item('site.title') : 'Bonfire'); ?>
			</a>
		</li>
		<li class="col-md-2" style="text-align:center;">
			<a href="<?php echo site_url(); ?>">
				<img src="<?php echo base_url('themes/default/images/logo-vp-vt.png'); ?>" alt="" width="100">
			</a>
		</li>
		<li class="col-md-5" style="text-align: right;padding-top: 10px;">
			<ul class="hi-menu">
				<li>
					<form method="get" action="<?php echo base_url('home/check_service_code');?>" class="form-inline">
						<input name="service_code" type="text" 
						value="<?php echo isset($_GET['service_code']) ? $_GET['service_code'] : ''; ?>" 
							class="form-control text-center" style="background: #fffefe;min-width: 250px;border-radius: 4px;">
						<button type="submit" class="btn btn-warning" style="height: 36px;">Tìm theo mã vé</button>
					</form>
				</li>
				<!-- <li class="dropdown">
				</li> -->
				<!-- <li class="dropdown">
					<a data-toggle="dropdown" href="#" aria-expanded="false"><i class="him-icon zmdi zmdi-more-vert"></i></a>
					<ul class="dropdown-menu dropdown-menu-lg dm-icon pull-right">
						<li class="skin-switch hidden-xs">
							<span class="ss-skin bgm-lightblue" data-ma-action="change-skin" data-ma-skin="lightblue"></span>
							<span class="ss-skin bgm-bluegray" data-ma-action="change-skin" data-ma-skin="bluegray"></span>
							<span class="ss-skin bgm-cyan" data-ma-action="change-skin" data-ma-skin="cyan"></span>
							<span class="ss-skin bgm-teal" data-ma-action="change-skin" data-ma-skin="teal"></span>
							<span class="ss-skin bgm-green" data-ma-action="change-skin" data-ma-skin="green"></span>
							<span class="ss-skin bgm-orange" data-ma-action="change-skin" data-ma-skin="orange"></span>
							<span class="ss-skin bgm-blue" data-ma-action="change-skin" data-ma-skin="blue"></span>
							<span class="ss-skin bgm-purple" data-ma-action="change-skin" data-ma-skin="purple"></span>
						</li>
						<li class="divider hidden-xs"></li>
						<li class="hidden-xs">
							<a data-ma-action="fullscreen" href="#"><i class="zmdi zmdi-fullscreen"></i> Toggle Fullscreen</a>
						</li>
						<li>
							<a data-ma-action="clear-localstorage" href="#"><i class="zmdi zmdi-delete"></i> Clear Local Storage</a>
						</li>
					</ul>
				</li> -->
				<?php if (isset($due_date_today)) { ?>
				<li class="hidden-xs ma-trigger" data-ma-action="sidebar-open" data-ma-target="#chat">
					<a href="#"><i class="him-icon zmdi zmdi-chart"></i></a>
				</li>
				<?php } ?>
			</ul>
		</li>
	</ul>
</header>



<script type="text/javascript">
	var notification_y_offset = 85;
	var user_current_id = '<?php echo isset($current_user->id) ? $current_user->id : '';?>';
</script>