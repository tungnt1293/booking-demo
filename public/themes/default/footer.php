<?php if ( ! isset($show) || $show == true) : ?>
	<footer id="footer" class="footer-alt">
		<p>Powered by <a href="" target="_blank">Tung Nguyen</a></p>
	</footer>
<?php endif; ?>

	<div class="page-loader">
		<div class="preloader pls-blue">
			<svg class="pl-circular" viewBox="25 25 50 50">
				<circle class="plc-path" cx="50" cy="50" r="20" />
			</svg>
			<p>Please wait...</p>
		</div>
	</div>
	<!-- Ajax Loader -->
	<div id="ajax-loader">
		<div class="preloader">
			<svg class="pl-circular" viewBox="25 25 50 50">
				<circle class="plc-path" cx="50" cy="50" r="20" />
			</svg>
			<p>Please wait...</p>
		</div>
	</div>	
	<div id="debug"><!-- Stores the Profiler Results --></div>
	<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo assets_path(); ?>vendors/bower_components/jquery/dist/jquery.min.js"><\/script>');</script>
	<?php echo Assets::js(); ?>
</body>
</html>