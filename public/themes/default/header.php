<?php

Assets::add_css([
	'vendors/bower_components/sweetalert2/dist/sweetalert2.min.css',
	'vendors/bower_components/animate.css/animate.min.css',
	'vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css',
	'vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css',
	'vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css',
	'vendors/bower_components/chosen/chosen.css',
	'app_1.min.css', 
	'app_2.min.css',
]);

Assets::add_js([
	'vendors/bower_components/bootstrap/dist/js/bootstrap.min.js',
	'vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
	'vendors/bower_components/Waves/dist/waves.min.js',
	'vendors/bootstrap-growl/bootstrap-growl.min.js',
	'vendors/bower_components/sweetalert2/dist/sweetalert2.min.js',
	'vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js',
	'vendors/bower_components/chosen/chosen.jquery.js',
	'app.min.js',
]);

?>
<!doctype html>
<head>
	<meta charset="utf-8">
	<title><?php
		echo isset($page_title) ? "{$page_title} : " : '';
		e(class_exists('Settings_lib') ? settings_item('site.title') : 'Bonfire');
	?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php e(isset($meta_description) ? $meta_description : ''); ?>">
	<meta name="author" content="<?php e(isset($meta_author) ? $meta_author : ''); ?>">
	<meta name="google-signin-client_id" content="500045433722-ipd0cld706tm52svb652fgs8aipd9c4n.apps.googleusercontent.com">

	<!--Modernizr is loaded before CSS so CSS can utilize its features-->
	<!--<script src="<?php echo js_path() . 'modernizr-2.5.3.js'; ?>"></script>-->

	<?php echo Assets::css(); ?>
	<link rel="shortcut icon" href="<?php echo base_url('favicon'); ?>/favicon.ico">

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('favicon'); ?>/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('favicon'); ?>/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('favicon'); ?>/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('favicon'); ?>/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('favicon'); ?>/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('favicon'); ?>/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('favicon'); ?>/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('favicon'); ?>/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('favicon'); ?>/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url('favicon'); ?>/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('favicon'); ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('favicon'); ?>/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('favicon'); ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo base_url('favicon'); ?>/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo base_url('favicon'); ?>/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
</head>
<body style="background:url(<?php echo base_url('themes/default/images/vinpearl_background.jpg'); ?>) no-repeat center center fixed;background-size: cover;">
<?php echo Template::message(); ?>
