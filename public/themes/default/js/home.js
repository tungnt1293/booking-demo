$(document).ready(function(){
    console.log('ready for home');
    $(document).on('change', '.service-select', function(e){
        var that = $(this);
        let service_selected = that.val();
        let price = that.find('option:selected').data('price').format(); 
        that.closest('.service-wrapper').find('.price').val(price);
        let quanlity = that.closest('.service-wrapper').find('.quantity').val();

        // Check duplicate service
        var k = 0; 
        $('.services-wrapper .service-wrapper .service-select').each(function(){
            let check_select = $(this).val();
            if (service_selected == check_select) {
                k++;
            }
        })

        if (k > 1) {
            my_message('Dịch vụ không được trùng nhau', 'error');
            that.val(0);
            that.closest('.service-wrapper').find('.price').val(0);
            return
        }

        // Check qtly
        if (quanlity < 1) {
            my_message('Số lượng phải lớn hơn 0', 'error');
            return
        }

        cal_total_bill();
    })

    $(document).on('change', '.quanlity', function() {
        if ($(this).val() < 1) {
            my_message('Số lượng phải lớn hơn 0', 'error');
            $(this).val(0);
            return;
        }

        cal_total_bill();
    })

    $(document).on('click', '.add-service', function(e) {
        e.preventDefault();
        // Add service
        add_service();
    })
    
    $(document).on('click', '.remove-service', function(e){
        e.preventDefault();
        $(this).parent().parent().remove();

        // re-index
        var i = 1; 
        $('.services-wrapper').find('.service-wrapper').each(function(){
            $(this).attr('id', i);
            $(this).find('.service-counting').text(i);
            i++;
        })

        cal_total_bill();
        $('.service-counting-total').text(i);
    })

    $(document).on('click', '.my-form [type=submit]', my_form_handler);

    $(document).on('change', '.vinpearl-select', function(){
        var that = $(this);
        var service_select = that.closest('.service-wrapper').find('.service-select');
        let vinpearl_id = that.val();
        var data = {
            'vinpearl_id': vinpearl_id,
        };
        data[ci_csrf_token_name] = ci_csrf_token();
        $.post(get_list_service_url, data, res => {
            res = JSON.parse(res);
            console.log(res);
            if (res['type'] == 'error') {
                $('#ajax-loader').fadeOut();
                my_message(res['message'], 'error');
                return
            }  else {
                service_select.empty();
                service_select.append('<option value="0" data-price="0">Chọn dịch vụ</option>');
                let list_service = JSON.parse(res['list_service']);
                $.each(list_service, function( index, value ) {
                    service_select.append(`
                        <option value="` + value['service_id'] + `" 
                        data-price="` + value['price'] + `">` + value['service_name'] + `</option>
                    `);
                });
            }

        }).fail((res) => {
            var error = /\((\d+), "(.*)"\)/.exec(res);
    
            if (/\((\d+), "(.*)"\)/.exec(res)) {
                message = `Error ${error[1]}: ${error[2]}`;
            } else {
                message = res.statusText;
            }
            my_message(message, 'error');
        });
    })

    var add_service = function() {
        let service_id = $('.services-wrapper').find('.service-wrapper').length + 1;
    
        $('.services-wrapper').append( $('#template_service').html().format(
            service_id
        ) );

        $('.service-counting-total').text(service_id);
    }

    var cal_total_bill = function() {
        var total_bill = 0;
        $('.services-wrapper .service-wrapper').each(function() {
            var price = $(this).find('.service-select option:selected').data('price');
            var quanlity = $(this).find('.quanlity').val();
            var this_service_price = price * quanlity;
            total_bill += this_service_price;
        })

        $('.total-bill').text(total_bill.format());
    }

    function my_form_handler(e = null) {
        e && e.preventDefault();
        var form = $('.my-form');

        var data = $(form).serializeObject();
        data[ci_csrf_token_name] = ci_csrf_token();
        $.post(submit_url, data, res => {
            console.log(res)
            res = JSON.parse(res);
            if (res['type'] == 'error') {
                $('#ajax-loader').fadeOut();
                my_message(res['message'], 'error');
                return
            }  else {
                my_message(res['message'], 'success');
                window.location.replace(res['redirect_url']);
            }

        }).fail((res) => {
            var error = /\((\d+), "(.*)"\)/.exec(res);
    
            if (/\((\d+), "(.*)"\)/.exec(res)) {
                message = `Error ${error[1]}: ${error[2]}`;
            } else {
                message = res.statusText;
            }
            my_message(message, 'error');
        });
    }

    function default_validator($form) {
        if ($form.find('input:invalid, select:invalid, input.invalid, select.invalid').length) {
            $form.find('input:invalid, select:invalid, input.invalid, select.invalid').focus();
            alert('Red input is required');
            return false;
        }
    
        return true;
    }

    function my_message(message, type = 'info', delay=8000) {
        $.bootstrapGrowl(message, {
            type: type,
            offset: {from: 'top', amount: 70},
            align: 'right',
            width: 400,
            delay: delay,
            allow_dismiss: true,
        });
    }

    var options = {
        render: 'image',
        minVersion: 1,
        maxVersion: 40,
        ecLevel: 'L',
        left: 0,
        top: 0,
        size: 250,
        fill: '# ',
        background: null,
        text: typeof(qr_text) != "undefined" && qr_text !== null ? qr_text : '',
        radius: 0,
        quiet: 0,
        mode: 0,
        mSize: 0.1,
        mPosX: 0.5,
        mPosY: 0.5,
        label: 'Vinpearl',
        fontname: 'sans',
        fontcolor: '#000',
        image: null
    }

    $('.qr-code').qrcode(options);

    Number.prototype.format = function(n, x) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
    };

    // Convert form data to an object, supports field array[1][2][3]...
    $.fn.serializeObject = function() {
        var d = $(this).serializeArray()
        res = {}
        for (i in d) {
            var regex_array = /\[(.*?)\]/g;
            var match = regex_array.exec(d[i].name);
            if (match) {
                var key = /^(.*?)\[/.exec(d[i].name)[1];
                if (!res[key])
                    res[key] = {};
                current_key = res[key];
                while (match) {
                    prev_key = current_key
                    prev_match = match[1];
                    if (!current_key[match[1]])
                        current_key[match[1]] = {}
                    current_key = current_key[match[1]]
                    match = regex_array.exec(d[i].name);
                }
                prev_key[prev_match] = d[i].value;
                continue;
            } 

            res[d[i].name] = d[i].value;
        }
        return res
    }
})